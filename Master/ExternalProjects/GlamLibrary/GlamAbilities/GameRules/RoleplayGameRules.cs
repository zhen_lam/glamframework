﻿using System;
using System.Collections.Generic;
using GlamLibrary.Events;
using GlamLibrary.Game.Managers;
using GlamLibrary.Events.EventTypes.NetworkEventArgs;

namespace GlamDynamicScripts.GameRules
{
    public class RoleplayGameRules : IGameRules, IEventReceiver
    {
        public int Priority
        {
            get
            {
                return 10;
            }
        }

        public void Init()
        {
            //hook events here
            EventsManager eventManager = GameManager.EventManager;
            eventManager.RegisterHook<ClientAuthorized>(OnClientAuthorized);
            eventManager.RegisterHook<ClientInfoChanged>(OnClientInfoChanged);
            eventManager.RegisterHook<ClientCommand>(OnClientCommand);
            eventManager.RegisterHook<ClientConnect>(OnConnect);
            eventManager.RegisterHook<ClientDisconnect>(OnDisconnect);
            eventManager.RegisterHook<ClientPutInServer>(OnPutInServer);
        }

        public void End()
        {
            EventsManager eventManager = GameManager.EventManager;
            eventManager.UnregisterHook<ClientAuthorized>(OnClientAuthorized);
            eventManager.UnregisterHook<ClientInfoChanged>(OnClientInfoChanged);
            eventManager.UnregisterHook<ClientCommand>(OnClientCommand);
            eventManager.UnregisterHook<ClientConnect>(OnConnect);
            eventManager.UnregisterHook<ClientDisconnect>(OnDisconnect);
            eventManager.UnregisterHook<ClientPutInServer>(OnPutInServer);
        }

        public EVENTRETURNTYPE OnClientAuthorized(ClientAuthorized args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnClientInfoChanged(ClientInfoChanged args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnClientCommand(ClientCommand args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnConnect(ClientConnect args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnDisconnect(ClientDisconnect args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnPutInServer(ClientPutInServer args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE OnSpawn()
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }
    }
}
