﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamLibrary.Events
{
    public interface IEventReceiver
    {
        int Priority { get; }
    }
}
