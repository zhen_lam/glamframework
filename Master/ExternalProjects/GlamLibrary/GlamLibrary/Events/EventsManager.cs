﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamLibrary.Events
{
    public enum EVENTRETURNTYPE
    {
        EVENT_CONTINUE,
        EVENT_HANDLED
    }

    public class EventsManager
    {
        public delegate EVENTRETURNTYPE EventCallback<T>(T arg) where T:EventArgs;
        private Dictionary<Type, List<Delegate>> eventListeners = new Dictionary<Type, List<Delegate>>();

        public EventsManager()
        {
        }

        public void RegisterHook<T>(EventCallback<T> callback) where T:EventArgs
        {
            Type type = typeof(T);
            if(!eventListeners.ContainsKey(type))
            {
                eventListeners.Add(type, new List<Delegate>());
            }
            eventListeners[type].Add(callback);
        }

        public void UnregisterHook<T>(EventCallback<T> callback) where T:EventArgs
        {
            Type type = typeof(T);
            if (!eventListeners.ContainsKey(type))
            {
                return;
            }
            eventListeners[type].Remove(callback);
        }

        public void BroadcastEvent<T>(T args) where T:EventArgs
        {
            Type type = typeof(T);
            if (!eventListeners.ContainsKey(type))
                return;

            List<Delegate> events = eventListeners[type];
            for (int i = 0; i < events.Count; i++)
            {
                EventCallback<T> callback = events[i] as EventCallback<T>;
                if (callback(args) == EVENTRETURNTYPE.EVENT_HANDLED)
                    break;
            }
        }
    }
}
