﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamLibrary.Events.EventTypes.GameEventArgs
{
    public class Spawn : EventArgs
    {
    }

    public class MovementInput : EventArgs
    {
        public float horizontal;
        public float vertical;

        public MovementInput(float h, float v)
        {
            horizontal = h;
            vertical = v;
        }
    }

    public class JumpInput : EventArgs
    {
    }

    public class PrimaryFireInput : EventArgs
    {
    }

    public class SecondaryFireInput : EventArgs
    {
    }

    public class ActivateAbility : EventArgs
    {
    }

    public class EndAbility : EventArgs
    {
    }
}
