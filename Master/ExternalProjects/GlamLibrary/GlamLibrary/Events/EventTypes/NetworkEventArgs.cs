﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamLibrary.Events.EventTypes.NetworkEventArgs
{
    public class ClientAuthorized : EventArgs
    {
    }

    public class ClientInfoChanged : EventArgs
    {
    }

    public class ClientCommand : EventArgs
    {
    }

    public class ClientConnect : EventArgs
    {
    }

    public class ClientDisconnect : EventArgs
    {
    }

    public class ClientPutInServer : EventArgs
    {
    }
}
