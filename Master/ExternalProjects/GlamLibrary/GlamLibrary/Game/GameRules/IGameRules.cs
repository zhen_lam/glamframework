﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamDynamicScripts.GameRules
{
    public interface IGameRules
    {
        void Init();
        void End();
    }
}
