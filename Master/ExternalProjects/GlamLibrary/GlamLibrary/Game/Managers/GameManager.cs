﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GlamLibrary.Events;
using GlamDynamicScripts.GameRules;

namespace GlamLibrary.Game.Managers
{
    public class GameManager
    {
        public static IGameRules GameRules { get; private set; }

        private static EventsManager eventManager;
        public static EventsManager EventManager
        {
            get 
            { 
                if (eventManager == null) eventManager = new EventsManager(); 
                    return eventManager; 
            }
        }
    }
}
