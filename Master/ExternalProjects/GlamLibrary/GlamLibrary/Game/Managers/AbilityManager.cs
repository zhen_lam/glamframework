﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GlamDynamicScripts.Abilities;
using GlamLibrary.Events.EventTypes.GameEventArgs;
using GlamLibrary.Events;

namespace GlamLibrary.Game.Managers
{
    public class AbilityManager
    {
        private List<AbilityBase> activeAbilities = new List<AbilityBase>();

        public EVENTRETURNTYPE ActiveAbility(ActivateAbility args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }

        public EVENTRETURNTYPE EndAbility(EndAbility args)
        {
            return EVENTRETURNTYPE.EVENT_CONTINUE;
        }
    }
}
