﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlamDynamicScripts.Abilities
{
    public abstract class AbilityBase
    {
        public virtual void Start()
        {
        }

        public virtual void Update(float deltaTime)
        {
        }

        public virtual void End()
        {
        }
    }
}
