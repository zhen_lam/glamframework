﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;
using System.IO;
using System.Security.Permissions;

namespace GlamLibrary
{
    public class ExternalScriptsLoader
    {
        private static FileSystemWatcher watcher;
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void Run(string directory)
        {
            watcher = new FileSystemWatcher();
            watcher.Path = directory;
            watcher.NotifyFilter = NotifyFilters.LastWrite;

            watcher.Filter = "*.cs";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;

        }

        // Define the event handlers.
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                watcher.EnableRaisingEvents = false;
            }
            finally
            {
                watcher.EnableRaisingEvents = true;
            }
        }
        public static Assembly CompileDirectory(string directoryPath)
        {
            string[] files = Directory.GetFiles(directoryPath, "*.cs");

            var compiler = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } });

            CompilerParameters compilerParams = new CompilerParameters();
            compilerParams.GenerateExecutable = false;
            compilerParams.GenerateInMemory = false;
            compilerParams.IncludeDebugInformation = false;
            compilerParams.TreatWarningsAsErrors = false;
            compilerParams.WarningLevel = 1;

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Assembly> distinctAssemblies = new List<Assembly>();

            for (int i = 0; i < assemblies.Length; i++)
            {
                bool hasAssembly = false;
                for (int j = 0; j < assemblies.Length; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    if (assemblies[i].FullName == assemblies[j].FullName)
                    {
                        hasAssembly = true;
                    }
                }
                if (!hasAssembly)
                {
                    distinctAssemblies.Add(assemblies[i]);
                }
            }
            for (int i = 0; i < distinctAssemblies.Count; i++)
            {
                try
                {
                    compilerParams.ReferencedAssemblies.Add(distinctAssemblies[i].Location);
                }
                catch (NotSupportedException)
                {

                }

            }
            CompilerResults results = compiler.CompileAssemblyFromFile(compilerParams, files);
            if (results.Errors.Count > 0)
            {
                for (int i = 0; i < results.Errors.Count; i++)
                {

                }
                return null;
            }
            else
            {
                return results.CompiledAssembly;
            }
        }
    }
}
