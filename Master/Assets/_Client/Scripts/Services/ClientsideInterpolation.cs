using UnityEngine;
using System.Collections.Generic;

public class ClientsideInterpolation : IService 
{
    public SyncState lastSyncState;
    public SyncState newSyncState;
    private ClientGame game;

    public ClientsideInterpolation(ClientGame game)
    {
        this.game = game;
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<NetworkEvents.SyncStateEvent>(OnSyncStateHandler);
    }
	
	// Update is called once per frame
	public void Update () 
    {
        if (newSyncState != null && lastSyncState != null)
        {
            foreach (KeyValuePair<int, Actor> kvp in newSyncState.actors)
            {
                if (kvp.Value != game.OwnerClient.Actor)
                {
                    kvp.Value.GameObject.transform.position = Vector3.Lerp(
                        kvp.Value.GameObject.transform.position, kvp.Value.position, Time.deltaTime * 10);
                }
            }
        }
	}

    private void OnSyncStateHandler(object sender, MessageBus<NetworkEvents.SyncStateEvent> message)
    {
        lastSyncState = newSyncState;
        newSyncState = message.data.SyncState;
    }

    public int Priority
    {
        get { return 3; }
    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate()
    {
    }

    public void End()
    {
        game.MessageBusManager.RemoveEventHandler<NetworkEvents.SyncStateEvent>(OnSyncStateHandler);
    }
}
