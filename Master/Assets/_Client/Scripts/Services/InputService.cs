using UnityEngine;
using System.Collections;

public class InputService : IService 
{
    public int Priority
    {
        get
        {
            return 1;
        }
    }

    private ClientGame game;
    private int inputId = 0;

    public InputService(ClientGame game)
    {
        this.game = game;
    }

    public void Start()
    {

    }

    public void Update()
    {
        if (game.OwnerClient == null || game.OwnerClient.Actor == null)
            return;

        float hor = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");
        bool jump = Input.GetButton("Jump");
        bool primaryFire = Input.GetButton("Fire1");
        bool secondaryFire = Input.GetButton("Fire2");

        InputEvents.Input input = new InputEvents.Input(
            game.OwnerClient, 
            inputId,
            Time.deltaTime,
            hor, 
            vert,
            jump,
            primaryFire,
            secondaryFire,
            game.OwnerClient.Actor.GameObject.transform.localEulerAngles);

        game.MessageBusManager.SendMessage(input);
        inputId++;
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void End()
    {
    }
}
