﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CameraService : IService
{
    public int Priority
    {
        get { return 5; }
    }

    private ClientGame game;
    private MasterCamera camera;
    public CameraService(ClientGame game)
    {
        this.game = game;
    }

    public void Start()
    {
        GameObject cam = GameObject.Instantiate(Resources.Load("Camera/MasterCameraRig")) as GameObject;
        camera = cam.GetComponent<MasterCamera>();
    }

    public void Update()
    {
        if (game.OwnerClient != null && game.OwnerClient.Actor != null && game.OwnerClient.Actor.GameObject != camera.player)
        {
            camera.SetPlayer(game.OwnerClient.Actor.GameObject);
        }
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void End()
    {
    }
}

