﻿using UnityEngine;
using System.Collections.Generic;
using Rapid.Tools;

/* Handle syncing managers, and actor data, and sending messages
 * 
 * 
 */
public class ClientsidePredictionService : IService
{
    public int Priority
    {
        get
        {
            return 0;
        }
    }

    private struct PosRot
    {
        public Vector3 pos;
        public Quaternion rot;
    }

    private CircularBuffer<InputEvents.Input> inputs = new CircularBuffer<InputEvents.Input>(100);
    private int lastAckInput;
    private PosRot lastAckPosRot;
    private PosRot correctPosRot;
    private float lastY;
    private ClientGame game;

    void Awake()
    {

    }


    void OnApplicationQuit()
    {
        // This will free up any memory that's been allocated and close all file streams etc.
        Graph.Dispose();
    }

    public ClientsidePredictionService(ClientGame game)
    {
        // Do a default setup. You can customize things such as where the log files should go if wanted.
        Graph.Initialize();


        // Just for fun, lets add a nice blue style:
        Graph.Instance.AddStyle(
            new GraphLogStyle("blue", new Color(0.5f, 0.5f, 1f), Color.cyan,
                new[] { new Color(0.5f, 0.2f, 0.92f, 1f), new Color(0.2f, 0.1f, 0.86f, 1f) })
        );

        // Create a log with labels and default style:
        Graph.Instance.CreateLog("deltatime_times_100", new[] { "delta", "smooth delta" });
        this.game = game;
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<InputEvents.Input>(OnInputHandler);
        game.MessageBusManager.AddEventHandler<NetworkEvents.SyncStateEvent>(OnSyncStateHandler);
    }

    public void Update()
    {
        if (game.OwnerClient == null || game.OwnerClient.Actor == null || game.OwnerClient.Actor.GameObject.GetComponent<PlayerMovement>() == null)
            return;

        PosRot currentPosRot = new PosRot();
        currentPosRot.pos = game.OwnerClient.Actor.GameObject.transform.position;
        currentPosRot.rot = game.OwnerClient.Actor.GameObject.transform.rotation;

        game.OwnerClient.Actor.GameObject.transform.position = lastAckPosRot.pos;
        game.OwnerClient.Actor.GameObject.transform.rotation = lastAckPosRot.rot;
        
        game.OwnerClient.Actor.GameObject.GetComponent<PlayerMovement>().moveDirection.y = lastY;
        InputEvents.Input[] inputEvents = inputs.ToArray();
        for (int i = 0; i < inputEvents.Length; i++)
        {
            InputEvents.Input currentInput = inputEvents[i];
            if (currentInput == null || currentInput.SyncID <= lastAckInput)
                continue;
            //do inputs
            game.OwnerClient.Actor.MessageBusManager.SendMessage(currentInput);

        }
        correctPosRot.pos = game.OwnerClient.Actor.GameObject.transform.position;
        correctPosRot.rot = game.OwnerClient.Actor.GameObject.transform.rotation;

        game.OwnerClient.Actor.GameObject.transform.position = currentPosRot.pos;
        game.OwnerClient.Actor.GameObject.transform.rotation = currentPosRot.rot;
    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate()
    {
        if (game.OwnerClient == null)
            return;

        game.OwnerClient.Actor.GameObject.transform.position = Vector3.Lerp(game.OwnerClient.Actor.GameObject.transform.position, correctPosRot.pos, Time.deltaTime * 10);
        if(correctPosRot.rot.x > 0)
            game.OwnerClient.Actor.GameObject.transform.rotation = Quaternion.Lerp(game.OwnerClient.Actor.GameObject.transform.rotation, correctPosRot.rot, Time.deltaTime * 10);
    }

    public void End()
    {

    }

    private void OnInputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        if (game.OwnerClient != null && game.OwnerClient.Actor != null)
        {
            game.OwnerClient.Actor.MessageBusManager.SendMessage(message.data);
        }
        inputs.InsertBackwards(message.data); 
        message.isConsumed = true;
    }

    private void OnSyncStateHandler(object sender, MessageBus<NetworkEvents.SyncStateEvent> message)
    {
        lastAckInput = game.OwnerClient.lastInputId;
        lastAckPosRot.pos = game.OwnerClient.Actor.position;
        lastAckPosRot.rot = game.OwnerClient.Actor.rotation;
        lastY = game.OwnerClient.Actor.Attributes[ActorAttributes.YVelocity];
    }
}
