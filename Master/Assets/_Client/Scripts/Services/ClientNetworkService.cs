﻿using UnityEngine;
using System.Collections.Generic;

/* Handle syncing managers, and actor data, and sending messages
 * 
 * 
 */
public class ClientNetworkService : IService
{
    public int Priority
    {
        get
        {
            return 0;
        }
    }

    private Game game;

    public ClientNetworkService(Game game)
    {
        this.game = game;
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<InputEvents.Input>(OnInputHandler);
        game.MessageBusManager.AddEventHandler<NetworkEvents.SyncStateEvent>(OnSyncStateHandler);
    }

    public void Update()
    {

    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate()
    {

    }

    public void End()
    {

    }

    private void OnInputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        uLink.BitStream stream = new uLink.BitStream(true);
        stream.Write(message.data.SyncID);
        stream.Write(message.data.DeltaTime);
        stream.Write(message.data.Horizontal);
        stream.Write(message.data.Vertical);
        stream.Write(message.data.Jump);
        stream.Write(message.data.PrimaryFire);
        stream.Write(message.data.SecondaryFire);
        stream.Write(message.data.ViewAngles);
        uLink.NetworkView.Get(game).RPC("S", uLink.RPCMode.Server, 'a', stream);
    }

    private void OnSyncStateHandler(object sender, MessageBus<NetworkEvents.SyncStateEvent> message)
    {
        uLink.NetworkView.Get(game).RPC("A", uLink.RPCMode.Server, message.data.SyncState.SyncId);
    }
}
