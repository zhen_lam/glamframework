using UnityEngine;
using System.Collections;

public class ClientGameState : IGameState
{
    private ClientGame game;
    private InputService inputService;
    private ClientNetworkService clientNetworkService;
    private ClientsidePredictionService clientsidePredictionService;
    private ClientsideInterpolation clientsideInterpolationService;
    private CameraService cameraService;

    public ClientGameState(ClientGame game)
    {
        this.game = game;
        inputService = new InputService(game);
        clientNetworkService = new ClientNetworkService(game);
        clientsidePredictionService = new ClientsidePredictionService(game);
        clientsideInterpolationService = new ClientsideInterpolation(game);
        cameraService = new CameraService(game);
    }

    public void Start()
    {
        game.AddService(inputService);
        game.AddService(clientNetworkService);
        game.AddService(clientsidePredictionService);
        game.AddService(clientsideInterpolationService);
        game.AddService(cameraService);
        game.ChangeState(new StartGameState(game));
    }

    public void Update()
    {
    }

    public void End()
    {
    }
}
