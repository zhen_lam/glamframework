﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ClientGame : Game
{
    public Client OwnerClient
    {
        get
        {
            if (GameManager.Clients.ContainsKey(uLink.Network.player))
            {
                return GameManager.Clients[uLink.Network.player];
            }
            return null;
        }
    }

    private bool allowReading = false;

    void Start()
    {
        Console.Instance.RegisterCommand("model", this, "ModelSwitch");
        uLink.Network.Connect("127.0.0.1", 7100);
    }

    public void uLink_OnConnectedToServer() 
    { 
        Debug.Log("Connected to server");
        string level = uLink.Network.approvalData.ReadString();
        LoadLevel(level, () =>
            {
                ChangeState(new ClientGameState(this));
                allowReading = true;
                Debug.Log("loaded level");
            });
        
    }

    public void uLink_OnSerializeNetworkView(uLink.BitStream stream, uLink.NetworkMessageInfo info)
    {
        if(stream.isReading && allowReading)
        {
            SyncState syncState = new SyncState(GameManager);
            syncState.Deserialize(stream);

            GameManager.Actors = syncState.actors;
            GameManager.Clients = syncState.clients;

            MessageBusManager.SendMessage(new NetworkEvents.SyncStateEvent(syncState));
        }
    }

    public void ModelSwitch(string modelName)
    {
        MessageBusManager.SendMessage<GameEvents.ModelSwap>(new GameEvents.ModelSwap(OwnerClient.Actor, modelName));

        uLink.BitStream stream = new uLink.BitStream(true);
        stream.WriteString(modelName);
        uLink.NetworkView.Get(this).RPC("S", uLink.RPCMode.Server, 'm', stream);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(), "");
    }
}

