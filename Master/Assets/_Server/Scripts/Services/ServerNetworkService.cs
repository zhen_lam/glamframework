using UnityEngine;
using System.Collections.Generic;

/* Handle syncing managers, and actor data, and sending messages
 * 
 * 
 */
public class ServerNetworkService : IService 
{
    public int Priority
    {
        get
        {
            return 0;
        }
    }

    private Game game;

    //keep a ring buffer of all the last game state snapshots
    public SyncRepository repository;
    private Dictionary<Client, int> lastAcks = new Dictionary<Client, int>();

    public ServerNetworkService(Game game)
    {
        this.game = game;
        repository = new SyncRepository(game.GameManager);
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<NetworkEvents.OnPlayerConnected>(OnPlayerConnected);
        game.MessageBusManager.AddEventHandler<NetworkEvents.OnPlayerDisconnected>(OnPlayerDisconnected);
        game.MessageBusManager.AddEventHandler<NetworkEvents.AckEvent>(OnAckHandler);
        game.MessageBusManager.AddEventHandler<InputEvents.Input>(OnInputHandler);
        game.MessageBusManager.AddEventHandler<NetworkEvents.SyncStateEvent>(OnSyncStateHandler);
    }

    public void Update()
    {

    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate()
    {

    }

    public void End()
    {

    }

    private void OnPlayerConnected(object sender, MessageBus<NetworkEvents.OnPlayerConnected> message)
    {
        Actor actor = ActorFactoryManager.CreateActor(game, "player");
        game.GameManager.Actors.Add(actor.Id, actor);

        Client client = new Client(game.GameManager);
        client.Actor = actor;
        client.NetworkPlayer = message.data.NetworkPlayer;

        game.GameManager.Clients.Add(client.NetworkPlayer, client);

        GameEvents.Spawn SpawnEvent = new GameEvents.Spawn(client.Actor);
        game.MessageBusManager.SendMessage(SpawnEvent);

        lastAcks.Add(client, 0);
    }

    private void OnPlayerDisconnected(object sender, MessageBus<NetworkEvents.OnPlayerDisconnected> message)
    {
        if (!game.GameManager.Clients.ContainsKey(message.data.NetworkPlayer))
        {
            return;
        }
        lastAcks.Remove(game.GameManager.Clients[message.data.NetworkPlayer]);
        GameObject.Destroy(game.GameManager.Clients[message.data.NetworkPlayer].Actor.GameObject);
        game.GameManager.Actors.Remove(game.GameManager.Clients[message.data.NetworkPlayer].Actor.Id);
        game.GameManager.Clients.Remove(message.data.NetworkPlayer);
    }

    private void OnAckHandler(object sender, MessageBus<NetworkEvents.AckEvent> message)
    {
        lastAcks[message.data.Client] = message.data.SyncID;
    }

    private void OnSyncStateHandler(object sender, MessageBus<NetworkEvents.SyncStateEvent> message)
    {
        repository.SyncStates.Insert(message.data.SyncState);
    }

    private void OnInputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        message.data.Client.lastInputId = message.data.SyncID;
    }

}
