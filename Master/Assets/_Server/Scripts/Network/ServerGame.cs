﻿using UnityEngine;
using System.Collections;
using uLink;

public class ServerGame : Game
{
    private ServerNetworkService networkService;
    // Use this for initialization
    void Start()
    {
        uLink.Network.InitializeServer(32, 7100);
        networkService = new ServerNetworkService(this);
        ChangeState(new ServerGameState(this, networkService));
    }

    void uLink_OnServerInitialized()
    {
        Debug.Log("Server initialized");
    }

    void uLink_OnPlayerApproval(uLink.NetworkPlayerApproval approval)
    {
        approval.Approve("UCP Demo Scene");
    }

    void uLink_OnPlayerConnected(uLink.NetworkPlayer player)
    {
        Debug.Log("Player connected from " + player.ipAddress + ":" + player.port);
        MessageBusManager.SendMessage(new NetworkEvents.OnPlayerConnected(player));
    }

    void uLink_OnPlayerDisconnected(uLink.NetworkPlayer player)
    {
        Debug.Log("Player disconnected from " + player.ipAddress + ":" + player.port);
        MessageBusManager.SendMessage(new NetworkEvents.OnPlayerDisconnected(player));
    }

    [RPC]
    void S(char id, uLink.BitStream stream, uLink.NetworkMessageInfo info)
    {
        switch (id)
        {
            case 'a':
                
                if (GameManager.Clients.ContainsKey(info.sender))
                {
                    MessageBusManager.SendMessage(new InputEvents.Input(
                        GameManager.Clients[info.sender],
                        stream.Read<int>(),
                        stream.Read<float>(),
                        stream.Read<float>(),
                        stream.Read<float>(),
                        stream.Read<bool>(),
                        stream.Read<bool>(),
                        stream.Read<bool>(),
                        stream.ReadVector3()));
                }
                break;
            case 'm':
                MessageBusManager.SendMessage<GameEvents.ModelSwap>(new GameEvents.ModelSwap(GameManager.Clients[info.sender].Actor, stream.ReadString()));
                break;
        }
    }

    [RPC]
    void A(int ack, uLink.NetworkMessageInfo info)
    {
        MessageBusManager.SendMessage(new NetworkEvents.AckEvent(GameManager.Clients[info.sender], ack));
    }

    public void uLink_OnSerializeNetworkView(uLink.BitStream stream, uLink.NetworkMessageInfo info)
    {
        if (stream.isWriting)
        {
            SyncState syncState = new SyncState(GameManager);
            syncState.Serialize(stream);
        }
    }
}
