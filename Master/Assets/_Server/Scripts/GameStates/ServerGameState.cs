using UnityEngine;
using System.Collections;

public class ServerGameState : IGameState
{
    private ServerGame game;
    private ServerNetworkService networkService;
    private ScoreboardRule scoreRule;
    private ZombieSpawnRule zombieSpawnRule;

    public ServerGameState(ServerGame game, ServerNetworkService networkService)
    {
        this.game = game;
        this.networkService = networkService;

        scoreRule = new ScoreboardRule(game);
        zombieSpawnRule = new ZombieSpawnRule(game);
    }

    public void Start()
    {
        game.AddService(networkService);


        game.LoadLevel("UCP Demo Scene", () =>
            {
                game.ChangeState(new StartGameState(game));
                game.AddService(scoreRule);
                game.AddService(zombieSpawnRule);
            });
    }

    public void Update()
    {
    }

    public void End()
    {
    }
}
