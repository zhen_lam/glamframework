using UnityEngine;
using System.Collections;
using System;

namespace NetworkEvents
{
    public class OnPlayerConnected : EventArgs
    {
        public uLink.NetworkPlayer NetworkPlayer { get; private set; }

        public OnPlayerConnected(uLink.NetworkPlayer player)
        {
            NetworkPlayer = player;
        }
    }

    public class OnPlayerDisconnected : EventArgs
    {
        public uLink.NetworkPlayer NetworkPlayer { get; private set; }

        public OnPlayerDisconnected(uLink.NetworkPlayer player)
        {
            NetworkPlayer = player;
        }
    }
}
