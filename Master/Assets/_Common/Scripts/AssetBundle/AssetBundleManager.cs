using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

static public class AssetBundleManager 
{
    static public string baseUrl = @"C:\";
    static public string baseLevelUrl = @"C:\";

    static public AssetMap Map { get; private set; }
    static public AssetMap LevelsMap { get; private set; }

   // A dictionary to hold the AssetBundle references
   static private Dictionary<string, AssetBundleRef> dictAssetBundleRefs;
   static private Dictionary<string, AssetBundleRef> dictLevelAssetBundleRefs;

   static AssetBundleManager ()
   {
       
#if UNITY_EDITOR
       baseUrl = @"file://" + Application.dataPath + "/_AssetBundles/";
       baseLevelUrl = @"file://" + Application.dataPath + "/_LevelAssetBundles/";
       Map = new AssetMap();
       DirectoryInfo di = new DirectoryInfo(Application.dataPath + "/_AssetBundles/");
       FileInfo[] files = di.GetFiles("*.json", SearchOption.AllDirectories);
       for (int i = 0; i < files.Length; i++)
       {
           using (StreamReader file = files[i].OpenText())
            {
              JsonSerializer serializer = new JsonSerializer();
              BundleManifest manifest = (BundleManifest)serializer.Deserialize(file, typeof(BundleManifest));
              for (int k = 0; k < manifest.AssetNames.Count; k++)
              {
                  AssetBundleData bundleData = new AssetBundleData();
                  bundleData.Name = manifest.AssetNames[k];
                  bundleData.Url = manifest.BundleName + "/" + manifest.BundleName;
                  bundleData.Version = manifest.Version;
                  Map.AssetBundles.Add(bundleData.Name, bundleData);
              }
            }
       }

       LevelsMap = new AssetMap();
       di = new DirectoryInfo(Application.dataPath + "/_LevelAssetBundles/");
       files = di.GetFiles("*.json", SearchOption.AllDirectories);
       for (int i = 0; i < files.Length; i++)
       {
           using (StreamReader file = files[i].OpenText())
           {
               JsonSerializer serializer = new JsonSerializer();
               LevelBundleManifest manifest = (LevelBundleManifest)serializer.Deserialize(file, typeof(LevelBundleManifest));
               AssetBundleData bundleData = new AssetBundleData();
               bundleData.Name = manifest.BundleName;
               bundleData.Url = manifest.BundleName + "/" + manifest.BundleName;
               bundleData.Version = manifest.Version;
               LevelsMap.AssetBundles.Add(bundleData.Name, bundleData);
               Debug.Log(bundleData.Name + " " + bundleData.Url);
           }
       }
       
#endif
       dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
       dictLevelAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
   }

   // Class with the AssetBundle reference, url and version
   private class AssetBundleRef 
   {
       public AssetBundle assetBundle = null;
       public int version;
       public string url;
       public AssetBundleRef(string strUrlIn, int intVersionIn)
       {
           url = strUrlIn;
           version = intVersionIn;
       }
   };

   public static IEnumerator DownloadLevelAsset(string name, Action<AssetBundle> callback)
   {
       if (LevelsMap == null)
       {
           using (WWW www = new WWW(baseUrl + "manifest.json"))
           {
               yield return www;
               if (www.error != null)
                   throw new Exception("WWW download:" + www.error);
               LevelsMap = JsonConvert.DeserializeObject<AssetMap>(www.text);
           }
       }

       if (!LevelsMap.AssetBundles.ContainsKey(name))
       {
           throw new Exception("LevelsMap missing key: " + name);
       }

       string keyName = LevelsMap.AssetBundles[name].Url + LevelsMap.AssetBundles[name].Version.ToString();
       if (dictLevelAssetBundleRefs.ContainsKey(keyName))
       {
           callback(dictLevelAssetBundleRefs[keyName].assetBundle);
           yield return null;
       }
       else
       {
           using (WWW www = WWW.LoadFromCacheOrDownload(baseLevelUrl + LevelsMap.AssetBundles[name].Url + ".unity3d", LevelsMap.AssetBundles[name].Version))
           {
               yield return www;
               if (www.error != null)
                   throw new Exception("WWW download:" + www.error);
               AssetBundleRef abRef = new AssetBundleRef(LevelsMap.AssetBundles[name].Url, LevelsMap.AssetBundles[name].Version);
               abRef.assetBundle = www.assetBundle;
               dictLevelAssetBundleRefs.Add(keyName, abRef);
               callback(abRef.assetBundle);
           }
       }
   }

   public static IEnumerator DownloadAsset(string name, Action<AssetBundle> callback)
   {
       if (Map == null)
       {
           using (WWW www = new WWW(baseUrl + "manifest.json"))
           {
               yield return www;
               if (www.error != null)
                   throw new Exception("WWW download:" + www.error);
               Map = JsonConvert.DeserializeObject<AssetMap>(www.text);
           }
       }

       if(!Map.AssetBundles.ContainsKey(name))
       {
           throw new Exception("AssetMap missing key: " + name);
       }

       string keyName = Map.AssetBundles[name].Url + Map.AssetBundles[name].Version.ToString();
       if (dictAssetBundleRefs.ContainsKey(keyName))
       {
           callback(dictAssetBundleRefs[keyName].assetBundle);
           yield return null;
       }
       else
       {
           using (WWW www = WWW.LoadFromCacheOrDownload(baseUrl + Map.AssetBundles[name].Url + ".unity3d", Map.AssetBundles[name].Version))
           {
               yield return www;
               if (www.error != null)
                   throw new Exception("WWW download:" + www.error);
               AssetBundleRef abRef = new AssetBundleRef(Map.AssetBundles[name].Url, Map.AssetBundles[name].Version);
               abRef.assetBundle = www.assetBundle;
               dictAssetBundleRefs.Add(keyName, abRef);
               callback(abRef.assetBundle);
           }
       }
   }

   public static AssetBundle GetAssetBundle(string name)
   {
       if (!Map.AssetBundles.ContainsKey(name))
       {
           throw new Exception("AssetMap missing key: " + name);
       }

       return GetAssetBundle(Map.AssetBundles[name].Url, Map.AssetBundles[name].Version);
   }

   // Get an AssetBundle
   public static AssetBundle GetAssetBundle (string url, int version)
   {
       string keyName = url + version.ToString();
       AssetBundleRef abRef;
       if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
           return abRef.assetBundle;
       else
           return null;
   }
	
   // Download an AssetBundle
   public static IEnumerator DownloadAssetBundle(string url, int version, Action<AssetBundle> callback)
   {
       string keyName = url + version.ToString();
       if (dictAssetBundleRefs.ContainsKey(keyName))
       {
           callback(dictAssetBundleRefs[keyName].assetBundle);
           yield return null;
       }
       else
       {
           using (WWW www = WWW.LoadFromCacheOrDownload(baseUrl + url, version))
           {
               yield return www;
               if (www.error != null)
                   throw new Exception("WWW download:" + www.error);
               AssetBundleRef abRef = new AssetBundleRef(url, version);
               abRef.assetBundle = www.assetBundle;
               callback(abRef.assetBundle);
               dictAssetBundleRefs.Add(keyName, abRef);
           }
       }
   }
	
   // Unload an AssetBundle
   public static void Unload (string url, int version, bool allObjects)
   {
       string keyName = url + version.ToString();
       AssetBundleRef abRef;
       if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
       {
           abRef.assetBundle.Unload (allObjects);
           abRef.assetBundle = null;
           dictAssetBundleRefs.Remove(keyName);
       }
   }
}