using UnityEngine;
using System.Collections.Generic;

public class LevelBundleManifest
{
    public string BundleName { get; set; }
    public int Version { get; set; }

    public LevelBundleManifest()
    {
    }
}

public class BundleManifest 
{
    public string BundleName { get; set; }
    public List<string> AssetNames { get; set; }
    public int Version { get; set; }

    public BundleManifest()
    {
        AssetNames = new List<string>();
    }
}
