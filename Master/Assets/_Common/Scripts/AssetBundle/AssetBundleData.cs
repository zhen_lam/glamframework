using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class AssetBundleData 
{
    public string Name { get; set; }
    [JsonProperty]
    public string Url { get; set; }
    [JsonProperty]
    public int Version { get; set; }
}
