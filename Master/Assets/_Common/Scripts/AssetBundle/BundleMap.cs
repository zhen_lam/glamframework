using UnityEngine;
using System.Collections.Generic;

public class BundleMapData
{
    public string BundleName { get; set; }
    public List<string> Assets { get; set; }

    public BundleMapData()
    {
        Assets = new List<string>();
    }
}

public class BundleMap
{
    public List<BundleMapData> Bundles { get; set; }

    public BundleMap()
    {
        Bundles = new List<BundleMapData>();
    }
}
