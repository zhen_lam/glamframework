using System.Collections.Generic;

public class AssetMap 
{
    public Dictionary<string, AssetBundleData> AssetBundles { get; set; }

    public AssetMap()
    {
        AssetBundles = new Dictionary<string, AssetBundleData>();
    }
}
