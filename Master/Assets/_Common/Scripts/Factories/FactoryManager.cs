using UnityEngine;
using System.Collections.Generic;

public class ActorFactoryManager 
{
    private static Dictionary<string, IFactory> factories = new Dictionary<string, IFactory>();

    static ActorFactoryManager()
    {
        new PlayerActorFactory();
        new ZombieActorFactory();
    }

    public static void RegisterFactory(string actorType, IFactory factory)
    {
        factories.Add(actorType, factory);
    }

    public static Actor CreateActor(Game game, string actorType)
    {
        return factories[actorType].Create(game, actorType);
    }

    public static Actor CreateActor(Game game, int id, string actorType)
    {
        return factories[actorType].Create(game, id, actorType);
    }
}
