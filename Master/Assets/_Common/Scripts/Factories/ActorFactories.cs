using UnityEngine;
using System.Collections;

public interface IFactory
{
    Actor Create(Game game, int id, string actorType);
    Actor Create(Game game, string actorType);
}

public class PlayerActorFactory : IFactory
{
    static PlayerActorFactory()
    {
        ActorFactoryManager.RegisterFactory("player", new PlayerActorFactory());
    }

    public Actor Create(Game game, string actorType)
    {
        Actor temp = new Actor(game, actorType);
        Compose(temp);
        return temp;
    }

    public Actor Create(Game game, int id, string actorType)
    {
        Actor temp = new Actor(game, id, actorType);
        Compose(temp);
        return temp;
    }

    void Compose(Actor temp)
    {
        temp.GameObject.AddComponent<PlayerModel>();
        temp.GameObject.AddComponent<PlayerMovement>();
        temp.GameObject.AddComponent<AbilityComponent>();
        temp.GameObject.AddComponent<TimelineComponent>();
        temp.GameObject.AddComponent<DamageComponent>();
    }
}

public class ZombieActorFactory : IFactory
{
    static ZombieActorFactory()
    {
        ActorFactoryManager.RegisterFactory("zombie", new PlayerActorFactory());
    }

    public Actor Create(Game game, string actorType)
    {
        Actor temp = new Actor(game, actorType);
        Compose(temp);
        return temp;
    }

    public Actor Create(Game game, int id, string actorType)
    {
        Actor temp = new Actor(game, id, actorType);
        Compose(temp);
        return temp;
    }

    void Compose(Actor temp)
    {
        temp.GameObject.AddComponent<PlayerModel>();
        temp.GameObject.AddComponent<AbilityComponent>();
        temp.GameObject.AddComponent<TimelineComponent>();
        temp.GameObject.AddComponent<DamageComponent>();
    }
}
