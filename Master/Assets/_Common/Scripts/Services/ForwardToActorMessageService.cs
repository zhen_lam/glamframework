using UnityEngine;
using System.Collections.Generic;

public class ForwardToActorMessageService : IService 
{
    public int Priority
    {
        get
        {
            return 0;
        }
    }

    private Game game;

    public ForwardToActorMessageService(Game game)
    {
        this.game = game;
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<InputEvents.Input>(InputHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.ModelSwap>(ModelSwapHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.ActivateAbilityEvent>(ActivateAbilityHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.InterruptAbilityEvent>(InterruptAbilityHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.TakeDamageEvent>(TakeDamageHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.DeathEvent>(DeathEventHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.CollisionTriggerEvent>(CollisionTriggerHandler);
    }

    public void Update()
    {

    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate()
    {

    }

    public void End()
    {
        game.MessageBusManager.RemoveEventHandler<InputEvents.Input>(InputHandler);
    }

    //event handlers
    void InputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        if (message.data.Client == null || message.data.Client.Actor == null)
            return;

        message.data.Client.Actor.MessageBusManager.SendMessage<InputEvents.Input>(message);
    }

    void ModelSwapHandler(object sender, MessageBus<GameEvents.ModelSwap> message)
    {
        if (message.data.Actor == null )
            return;

        message.data.Actor.MessageBusManager.SendMessage<GameEvents.ModelSwap>(message);
    }

    void ActivateAbilityHandler(object sender, MessageBus<GameEvents.ActivateAbilityEvent> message)
    {
        if (message.data.Actor == null)
            return;

        message.data.Actor.MessageBusManager.SendMessage<GameEvents.ActivateAbilityEvent>(message);
    }

    void InterruptAbilityHandler(object sender, MessageBus<GameEvents.InterruptAbilityEvent> message)
    {
        if (message.data.Actor == null)
            return;

        message.data.Actor.MessageBusManager.SendMessage<GameEvents.InterruptAbilityEvent>(message);
    }

    void TakeDamageHandler(object sender, MessageBus<GameEvents.TakeDamageEvent> message)
    {
        if (message.data.Receiver == null)
            return;

        message.data.Receiver.MessageBusManager.SendMessage<GameEvents.TakeDamageEvent>(message);
    }

    void DeathEventHandler(object sender, MessageBus<GameEvents.DeathEvent> message)
    {
        if (message.data.Receiver == null)
            return;

        message.data.Receiver.MessageBusManager.SendMessage<GameEvents.DeathEvent>(message);
    }

    void CollisionTriggerHandler(object sender, MessageBus<GameEvents.CollisionTriggerEvent> message)
    {
        if (message.data.Actor == null)
            return;

        message.data.Actor.MessageBusManager.SendMessage<GameEvents.CollisionTriggerEvent>(message);
    }
}
