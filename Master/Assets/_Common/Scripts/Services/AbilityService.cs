using UnityEngine;
using System.Collections;

public class AbilityService : IService
{
    public int Priority
    {
        get { return 3; }
    }

    private Game game;

    public AbilityService(Game game)
    {
        this.game = game;
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<GameEvents.ActivateAbilityEvent>(ActivateAbilityEventHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.EndTimelineEvent>(EndAbilityEventHandler);
        game.MessageBusManager.AddEventHandler<GameEvents.InterruptAbilityEvent>(InterruptAbilityEventHandler);
    }

    public void Update()
    {
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void End()
    {
        game.MessageBusManager.RemoveEventHandler<GameEvents.ActivateAbilityEvent>(ActivateAbilityEventHandler);
        game.MessageBusManager.RemoveEventHandler<GameEvents.EndTimelineEvent>(EndAbilityEventHandler);
        game.MessageBusManager.RemoveEventHandler<GameEvents.InterruptAbilityEvent>(InterruptAbilityEventHandler);
    }

    private void ActivateAbilityEventHandler(object sender, MessageBus<GameEvents.ActivateAbilityEvent> message)
    {
        message.data.Actor.MessageBusManager.SendMessage<GameEvents.ActivateAbilityEvent>(message);
    }

    private void EndAbilityEventHandler(object sender, MessageBus<GameEvents.EndTimelineEvent> message)
    {
        message.data.Actor.MessageBusManager.SendMessage<GameEvents.EndTimelineEvent>(message);
    }

    private void InterruptAbilityEventHandler(object sender, MessageBus<GameEvents.InterruptAbilityEvent> message)
    {
        message.data.Actor.MessageBusManager.SendMessage<GameEvents.InterruptAbilityEvent>(message);
    }
}
