using UnityEngine;
using System.Collections.Generic;

public class SpawnService : IService 
{
    public int Priority
    {
        get
        {
            return 1;
        }
    }

    private Game game;
    private List<Spawner> spawners = new List<Spawner>();

    public SpawnService(Game game)
    {
        this.game = game;
        game.MessageBusManager.AddEventHandler<GameEvents.Spawn>(SpawnEventHandler);
    }

    private void SpawnEventHandler(object sender, MessageBus<GameEvents.Spawn> message)
    {
        int rand = Random.Range(0, spawners.Count);
        Spawner spawner = spawners[rand];

        Actor actor = message.data.Actor;
        Debug.Log(actor.Id);
        Debug.Log(actor.GameObject.name);
        actor.GameObject.transform.position = spawner.transform.position;
        actor.GameObject.transform.rotation = spawner.transform.rotation;
    }

    private void GrabAllSpawnersInScene()
    {
        Spawner[] spawners = GameObject.FindSceneObjectsOfType(typeof(Spawner)) as Spawner[];
        this.spawners = new List<Spawner>(spawners);
    }

    public void Start()
    {
        GrabAllSpawnersInScene();

    }

    public void Update()
    {
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void End()
    {
    }
}
