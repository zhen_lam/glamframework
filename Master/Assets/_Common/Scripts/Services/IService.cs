using UnityEngine;
using System.Collections;

public interface IService
{
    int Priority
    {
        get;
    }

    void Start();
    void Update();
    void FixedUpdate();
    void LateUpdate();
    void End();
}
