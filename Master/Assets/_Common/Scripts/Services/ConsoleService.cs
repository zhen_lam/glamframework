using UnityEngine;
using System.Collections;

public class ConsoleService : IService 
{
    public int Priority
    {
        get { return 3; }
    }

    private Game game;

    public ConsoleService(Game game)
    {
        this.game = game;
    }

    public void Start()
    {
        
    }

    public void Update()
    {
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void End()
    {
    }


}
