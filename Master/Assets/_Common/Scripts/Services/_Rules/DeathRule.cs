using UnityEngine;
using System.Collections;

public class DeathRule : IService 
{
    private Game game;

    public DeathRule(Game game)
    {
        this.game = game;
    }

    public int Priority
    {
        get { return 0; }
    }

    public void Start()
    {
        game.MessageBusManager.AddEventHandler<GameEvents.DeathEvent>(DeathEventHandler);
    }

    void DeathEventHandler(object sender, MessageBus<GameEvents.DeathEvent> message)
    {
        //set respawn time
    }

    public void Update()
    {
        
    }

    public void FixedUpdate()
    {
       
    }

    public void LateUpdate()
    {
        
    }

    public void End()
    {
        game.MessageBusManager.RemoveEventHandler<GameEvents.DeathEvent>(DeathEventHandler);
    }
}
