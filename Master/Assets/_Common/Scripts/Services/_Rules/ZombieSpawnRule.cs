using UnityEngine;
using System.Collections.Generic;

public class ZombieSpawnRule : IService 
{
    private Game game;

    private List<ZombieSpawner> spawners = new List<ZombieSpawner>();
    private int wave = 0;
    private float waveSpawnTime = 60;
    private float nextSpawnTime = 0;

    public ZombieSpawnRule(Game game)
    {
        this.game = game;
    }

    public int Priority
    {
        get { return 0; }
    }

    public void Start()
    {
        ZombieSpawner[] spawners = GameObject.FindSceneObjectsOfType(typeof(ZombieSpawner)) as ZombieSpawner[];
        this.spawners = new List<ZombieSpawner>(spawners);
    }

    public void Update()
    {
        if (wave == 0)
        {
            nextSpawnTime = Time.time;
        }

        if (Time.time >= nextSpawnTime)
        {
            Spawn();
            wave++;
            nextSpawnTime = Time.time + waveSpawnTime;
        }
    }

    public void FixedUpdate()
    {
       
    }

    public void LateUpdate()
    {
        
    }

    public void End()
    {
        
    }

    void Spawn()
    {
        for (int i = 0; i < 10; i++)
        {
            Actor actor = ActorFactoryManager.CreateActor(game, "zombie");
            game.GameManager.Actors.Add(actor.Id, actor);

            int rand = Random.Range(0, spawners.Count);
            ZombieSpawner spawner = spawners[rand];

            Debug.Log(actor.Id);
            Debug.Log(actor.GameObject.name);
            actor.GameObject.transform.position = spawner.transform.position;
            actor.GameObject.transform.rotation = spawner.transform.rotation;
        }
    }
}
