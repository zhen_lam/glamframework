using UnityEngine;
using System.Collections;

public class ScoreboardRule : IService 
{
    private Game game;

    public ScoreboardRule(Game game)
    {
        this.game = game;
        game.MessageBusManager.AddEventHandler<GameEvents.DeathEvent>(DeathEventHandler);
    }

    public int Priority
    {
        get { return 0; }
    }

    void DeathEventHandler(object sender, MessageBus<GameEvents.DeathEvent> message)
    {
        game.Attributes[GameAttributes.Kills, message.data.Attacker.Id]++;
        game.Attributes[GameAttributes.Deaths, message.data.Attacker.Id]++;
    }

    public void Start()
    {
        
    }

    public void Update()
    {
        
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
        
    }

    public void End()
    {
        game.MessageBusManager.RemoveEventHandler<GameEvents.DeathEvent>(DeathEventHandler);
    }
}
