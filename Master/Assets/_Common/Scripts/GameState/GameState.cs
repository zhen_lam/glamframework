using UnityEngine;
using System.Collections;

public interface IGameState 
{
    void Start();
    void Update();
    void End();
}
