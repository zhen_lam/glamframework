﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class StartGameState : IGameState
{
    private Game game;
    private SpawnService spawnService;
    private ForwardToActorMessageService rulesService;
    private DeathRule deathRule;


    public StartGameState(Game game)
    {
        this.game = game;
        spawnService = new SpawnService(game);
        rulesService = new ForwardToActorMessageService(game);
        deathRule = new DeathRule(game);

    }

    public void Start()
    {
        game.AddService(spawnService);
        game.AddService(rulesService);
        game.AddService(deathRule);

    }

    public void Update()
    {
        
    }

    public void End()
    {
        game.RemoveService(spawnService);
        game.RemoveService(rulesService);
    }
}

