using UnityEngine;
using System.Collections;
using System;

public class GameAttribute<T> : GameAttribute
{
    public GameAttribute(int id)
    {
        this.Id = id;
        this.Type = typeof(T).TypeHandle;
        this.defaultValue = default(T);
    }

    public GameAttribute(int id, T defaultValue)
    {
        this.Id = id;
        this.Type = typeof(T).TypeHandle;
        this.defaultValue = defaultValue;
    }
}

public class GameAttribute
{
    public int Id { get; protected set; }
    public RuntimeTypeHandle Type { get; protected set; }
    public object defaultValue;
}
