using System.Collections.Generic;

public class AttributeMap : IULinkSerializeable
{
    private Dictionary<GameAttribute, Dictionary<int, object>> map = new Dictionary<GameAttribute, Dictionary<int, object>>();

    public int this[GameAttribute<int> attribute, int index = 0]
    {
        get
        {
            return GetAttributeValue(attribute, index);
        }
        set
        {
            AddAttributeValue(attribute, index, value);
        }
    }

    public bool this[GameAttribute<bool> attribute, int index = 0]
    {
        get
        {
            return GetAttributeValue(attribute, index);
        }
        set
        {
            AddAttributeValue(attribute, index, value);
        }
    }

    public string this[GameAttribute<string> attribute, int index = 0]
    {
        get
        {
            return GetAttributeValue(attribute, index);
        }
        set
        {
            AddAttributeValue(attribute, index, value);
        }
    }

    public float this[GameAttribute<float> attribute, int index = 0]
    {
        get
        {
            return GetAttributeValue(attribute,index);
        }
        set
        {
            AddAttributeValue(attribute, index, value);
        }
    }

    public T GetAttributeValue<T>(GameAttribute<T> attribute, int index)
    {
        if (!map.ContainsKey(attribute))
        {
            Dictionary<int, object> temp = new Dictionary<int, object>();
            temp.Add(index, attribute.defaultValue);
            map.Add(attribute, temp);
            return (T)attribute.defaultValue;
        }
        return (T)map[attribute][index];
    }

    public void AddAttributeValue(GameAttribute attribute, int index, object value)
    {
        if (value == null)
        {
            RemoveAttributeValue(attribute, index);
        }
        else
        {
            if (!map.ContainsKey(attribute))
            {
                Dictionary<int, object> temp = new Dictionary<int, object>();
                temp.Add(index, value);
                map.Add(attribute, temp);
            }
            else if (!map[attribute].ContainsKey(index))
            {
                map[attribute].Add(index, null);
            }
            map[attribute][index] = value;
        }
    }

    public void RemoveAttributeValue(GameAttribute attribute, int index)
    {
        if (map.ContainsKey(attribute) && map[attribute].ContainsKey(index))
        {
            map[attribute].Remove(index);
        }
    }

    public void Serialize(uLink.BitStream stream)
    {
        int count = map.Count;
        stream.Write(count);
        foreach (KeyValuePair<GameAttribute, Dictionary<int, object>> kvp in map)
        {
            stream.Write(kvp.Key.Id);
            stream.Write(kvp.Value.Count);
            foreach (KeyValuePair<int, object> attributekvp in kvp.Value)
            {
                stream.Write(attributekvp.Key);
                stream.WriteObject(kvp.Key.Type, attributekvp.Value);
            }
        }
    }

    public void Deserialize(uLink.BitStream stream)
    {
        Dictionary<GameAttribute, Dictionary<int, object>> map = new Dictionary<GameAttribute, Dictionary<int, object>>();
        int count = stream.Read<int>();
        for (int i = 0; i < count; i++)
        {
            int id = stream.Read<int>();
            int attributecount = stream.Read<int>();
            GameAttribute attribute = ActorAttributes.Attributes[id];
            for (int j = 0; j < attributecount; j++)
            {
                int key = stream.Read<int>();
                object value = stream.ReadObject(attribute.Type);

                Dictionary<int, object> temp = new Dictionary<int, object>();
                temp.Add(key, value);
                map.Add(attribute, temp);

            }
        }
        this.map = map;
    }

    public void AddAttributeFromAttributeData(Dictionary<int, GameAttribute> Attributes, AttributeData data)
    {
        //TODO: map attribute types to dictionary
        //TODO: find a way to choose which attribute map to use
        GameAttribute attribute = Attributes[data.attributeData];
        if (attribute is GameAttribute<int>)
        {
            AddAttributeValue(attribute, 0, data.value);
        }
        else if (attribute is GameAttribute<float>)
        {
            AddAttributeValue(attribute, 0, data.valueF);
        }
        else if (attribute is GameAttribute<string>)
        {
            AddAttributeValue(attribute, 0, data.valueS);
        }
    }
}

