using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class ActorAttributes
{
    public static Dictionary<int, GameAttribute> Attributes = new Dictionary<int, GameAttribute>();

    static ActorAttributes()
    {
        Attributes = typeof(ActorAttributes).GetFields()
                        .Where(a => {
                            return a.IsStatic &&
                            (a.FieldType.BaseType == typeof(GameAttribute));
                        })
                        .Select(a => a.GetValue(null) as GameAttribute)
                        .ToDictionary(a => a.Id, a => a);
    }

    public static readonly GameAttribute<int> MaxHealth = new GameAttribute<int>(0, 100);
    public static readonly GameAttribute<int> Health = new GameAttribute<int>(1, 100);
    public static readonly GameAttribute<float> MaxSpeed = new GameAttribute<float>(2, 5);
    public static readonly GameAttribute<float> Speed = new GameAttribute<float>(3, 5);
    public static readonly GameAttribute<float> JumpSpeed = new GameAttribute<float>(4, 5);
    public static readonly GameAttribute<float> Gravity = new GameAttribute<float>(5, 10);
    public static readonly GameAttribute<string> Model = new GameAttribute<string>(6, "");
    public static readonly GameAttribute<float> YVelocity = new GameAttribute<float>(7);
    public static readonly GameAttribute<float> Timeline = new GameAttribute<float>(8);
}

public static class GameAttributes
{
    public static Dictionary<int, GameAttribute> Attributes = new Dictionary<int, GameAttribute>();

    static GameAttributes()
    {
        Attributes = typeof(ActorAttributes).GetFields()
                        .Where(a =>
                        {
                            return a.IsStatic &&
                            (a.FieldType.BaseType == typeof(GameAttribute));
                        })
                        .Select(a => a.GetValue(null) as GameAttribute)
                        .ToDictionary(a => a.Id, a => a);
    }

    public static readonly GameAttribute<int> Kills = new GameAttribute<int>(0, 0);
    public static readonly GameAttribute<int> Deaths = new GameAttribute<int>(1, 0);
}

public static class TimelineAnimationAttributes
{
    public static Dictionary<int, GameAttribute> Attributes = new Dictionary<int, GameAttribute>();

    static TimelineAnimationAttributes()
    {
        Attributes = typeof(TimelineAnimationAttributes).GetFields()
                                .Where(a =>
                                            a.IsStatic &&
                                            (a.FieldType.BaseType == typeof(GameAttribute)))
                                .Select(a => a.GetValue(null) as GameAttribute)
                                .ToDictionary(a => a.Id, a=>a);
    }

    public static readonly GameAttribute<string> Name = new GameAttribute<string>(0);
    public static readonly GameAttribute<float> Speed = new GameAttribute<float>(1,1);
    public static readonly GameAttribute<float> Length = new GameAttribute<float>(2);
    public static readonly GameAttribute<int> Layer = new GameAttribute<int>(3, 0);
    public static readonly GameAttribute<int> Weight = new GameAttribute<int>(4, 1);
    public static readonly GameAttribute<int> Wrapmode = new GameAttribute<int>(5, 0);
    public static readonly GameAttribute<int> Blendmode = new GameAttribute<int>(6, 0);
    public static readonly GameAttribute<float> FadeinTime = new GameAttribute<float>(7, 0.3f);
    public static readonly GameAttribute<float> FadeoutTime = new GameAttribute<float>(8, 0.3f);
}

public static class TimelineCollisionAttributes
{
    public static Dictionary<int, GameAttribute> Attributes = new Dictionary<int, GameAttribute>();

    static TimelineCollisionAttributes()
    {
        Attributes = typeof(TimelineCollisionAttributes).GetFields()
                                .Where(a =>
                                            a.IsStatic &&
                                            (a.FieldType.BaseType == typeof(GameAttribute)))
                                .Select(a => a.GetValue(null) as GameAttribute)
                                .ToDictionary(a => a.Id, a => a);
    }

    public static readonly GameAttribute<int> CollisionType = new GameAttribute<int>(0);
}