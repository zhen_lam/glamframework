using UnityEngine;
using System.Collections;
using System;

namespace InputEvents
{
    public class Input : EventArgs
    {
        public Client Client { get; private set; }
        public int SyncID { get; private set; }
        public float DeltaTime { get; private set; }
        public float Horizontal { get; private set; }
        public float Vertical { get; private set; }
        public bool Jump { get; set; }
        public bool PrimaryFire { get; private set; }
        public bool SecondaryFire { get; private set; }
        public Vector3 ViewAngles { get; private set; }

        public bool isGrounded = false;

        public Input(
            Client client,
            int syncId,
            float deltaTime,
            float hor,
            float vert,
            bool jump,
            bool primaryFire,
            bool secondaryFire,
            Vector3 viewAngles)
        {
            Client = client;
            SyncID = syncId;
            DeltaTime = deltaTime;
            Horizontal = hor;
            Vertical = vert;
            Jump = jump;
            PrimaryFire = primaryFire;
            SecondaryFire = secondaryFire;
            ViewAngles = viewAngles;
        }

        public Input(Input input)
        {
            Client = input.Client;
            SyncID = input.SyncID;
            DeltaTime = input.DeltaTime;
            Horizontal = input.Horizontal;
            Vertical = input.Vertical;
            Jump = input.Jump;
            PrimaryFire = input.PrimaryFire;
            SecondaryFire = input.SecondaryFire;
            ViewAngles = input.ViewAngles;
        }
    }

    public class Move: EventArgs
    {
        public Client Client { get; private set; }
        public float Horizontal { get; private set; }
        public float Vertical { get; private set; }

        public Move(Client client, float horizontal, float vertical)
        {
            Client = client;
            Horizontal = horizontal;
            Vertical = vertical;
        }
    }

    public class Jump : EventArgs
    {
        public Client Client { get; private set; }

        public Jump(Client client)
        {
            Client = client;
        }
    }
}

namespace GameEvents
{
    public class Spawn : EventArgs
    {
        public Actor Actor { get; private set; }

        public Spawn(Actor actor)
        {
            Actor = actor;
        }
    }

    public class ModelSwap : EventArgs
    {
        public Actor Actor { get; private set; }
        public string Model { get; private set; }

        public ModelSwap(Actor actor, string model)
        {
            Actor = actor;
            Model = model;
        }
    }

    public class PlayTimelineEvent : EventArgs
    {
        public float StartTime { get; private set; }
        public Actor Actor { get; private set; }
        public TimelineData Timeline { get; private set; }

        public PlayTimelineEvent(float startTime, Actor actor, TimelineData timeline)
        {
            StartTime = startTime;
            Actor = actor;
            Timeline = timeline;
        }
    }

    public class EndTimelineEvent : EventArgs
    {
        public Actor Actor { get; private set; }
        public TimelineData Timeline { get; private set; }

        public EndTimelineEvent(Actor actor, TimelineData timeline)
        {
            Actor = actor;
            Timeline = timeline;
        }
    }

    public class ActivateAbilityEvent : EventArgs
    {
        public Actor Actor { get; private set; }
        public AbilityData Ability { get; private set; }

        public ActivateAbilityEvent(Actor actor, AbilityData ability)
        {
            Actor = actor;
            Ability = ability;
        }
    }

    public class InterruptAbilityEvent : EventArgs
    {
        public Actor Actor { get; private set; }
        public AbilityData Ability { get; private set; }

        public InterruptAbilityEvent(Actor actor, AbilityData ability)
        {
            Actor = actor;
            Ability = ability;
        }
    }

    public class TakeDamageEvent : EventArgs
    {
        public Actor Attacker { get; private set; }
        public Actor Receiver { get; private set; }
        public int Damage { get; set; }

        public TakeDamageEvent(Actor attacker, Actor receiver, int damage)
        {
            Attacker = attacker;
            Receiver = receiver;
            Damage = damage;
        }
    }

    public class DeathEvent : EventArgs
    {
        public Actor Attacker { get; private set; }
        public Actor Receiver { get; private set; }

        public DeathEvent(Actor attacker, Actor receiver)
        {
            Attacker = attacker;
            Receiver = receiver;
        }
    }

    public class CollisionTriggerEvent : EventArgs
    {
        public Actor Actor { get; private set; }
        public CollisionMapEnum ColliderType { get; private set; }
        public bool Enabled { get; private set; }

        public CollisionTriggerEvent(Actor actor, CollisionMapEnum colliderType, bool enabled)
        {
            Actor = actor;
            ColliderType = colliderType;
            Enabled = enabled;
        }
    }
}

namespace NetworkEvents
{
    public class SerializeNetwork : EventArgs
    {
        public uLink.BitStream Bitstream;

        public SerializeNetwork(uLink.BitStream bitstream)
        {
            Bitstream = bitstream;
        }
    }

    public class DeserializeNetwork : EventArgs
    {
        public uLink.BitStream Bitstream;

        public DeserializeNetwork(uLink.BitStream bitstream)
        {
            Bitstream = bitstream;
        }
    }

    public class SyncStateEvent : EventArgs
    {
        public SyncState SyncState { get; private set; }

        public SyncStateEvent(SyncState state)
        {
            SyncState = state;
        }
    }

    public class AckEvent : EventArgs
    {
        public Client Client { get; private set; }
        public int SyncID { get; private set; }

        public AckEvent(Client client, int id)
        {
            Client = client;
            SyncID = id;
        }
    }
}

namespace AnimationEvents
{
    public class AnimationEvent : EventArgs
    {
        public string AnimationName { get; private set; }
        public float Speed { get; private set; }
        public int Layer { get; private set; }
        public WrapMode WrapMode { get; private set; }
        public float Weight { get; private set; }
        public AnimationBlendMode BlendMode { get; private set; }
        public float FadeInTime { get; private set; }

        public AnimationEvent(string name, float speed, int layer, float weight, WrapMode wrapmode, AnimationBlendMode blendmode, float fadeinTime)
        {
            AnimationName = name;
            Speed = speed;
            Layer = layer;
            Weight = weight;
            WrapMode = wrapmode;
            BlendMode = blendmode;
            FadeInTime = fadeinTime;
        }
    }

    public class SetAnimationTimeEvent : EventArgs
    {
        public string AnimationName { get; private set; }
        public float Time { get; private set; }

        public SetAnimationTimeEvent(string animationName, float time)
        {
            AnimationName = animationName;
            Time = time;
        }
    }

    public class EndAnimationEvent : EventArgs
    {
        public string AnimationName { get; private set; }
        public float FadeOutTime { get; private set; }

        public EndAnimationEvent(string animationName, float fadeoutTime)
        {
            AnimationName = animationName;
            FadeOutTime = fadeoutTime;
        }
    }
}
