using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using uLink;
using System;

public class Game : uLink.MonoBehaviour 
{
    public static Game Instance { get; private set; }
    public MessageBusManager MessageBusManager { get; private set; }
    public GameManager GameManager { get; private set; }
    public AttributeMap Attributes { get; private set; }

    public static List<IService> Services { get; private set; }
    public static IGameState CurrentState { get; private set; }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        uLink.Network.isAuthoritativeServer = true;
        Instance = this;
        MessageBusManager = new MessageBusManager();
        GameManager = new GameManager(this);
        Services = new List<IService>();
        AddService(new ConsoleService(this));
    }

    public void AddService(IService service)
    {
        Services.Add(service);
        service.Start();
    }

    public void RemoveService(IService service)
    {
        if (Services.Remove(service))
        {
            service.End();
        }
    }

    public void ChangeState(IGameState state)
    {
        if (CurrentState != null)
        {
            CurrentState.End();
        }
        CurrentState = state;

        if (CurrentState != null)
        {
            state.Start();
        }
    }

	void Start()
    {
        for (int i = 0; i < Services.Count; i++)
        {
            Services[i].Start();
        }
	}

    void Update() 
    {
        for (int i = 0; i < Services.Count; i++)
        {
            Services[i].Update();
        }
        MessageBusManager.ProcessMessages();
	}

    void FixedUpdate()
    {
        for (int i = 0; i < Services.Count; i++)
        {
            Services[i].FixedUpdate();
        }
    }

    void LateUpdate()
    {
        for (int i = 0; i < Services.Count; i++)
        {
            Services[i].LateUpdate();
        }
    }

    public void LoadLevel(string level, Action onLoadComplete)
    {
        StartCoroutine(AssetBundleManager.DownloadLevelAsset(level, (bundle)=>
            {
                bundle.LoadAll();
                StartCoroutine(LoadLevelCoroutine(level, onLoadComplete));
            }));
    }

    public IEnumerator LoadLevelCoroutine(string level, Action onLoadComplete)
    {
        Application.LoadLevel(level);
        yield return null;
        yield return null;
        onLoadComplete();
    }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("level was loaded");

    }
}
