using UnityEngine;
using System.Collections;
using uLink;

/*
 * Holds client state
 */
public class Client : IULinkSerializeable
{
    //account data
    public uLink.NetworkPlayer NetworkPlayer;

    private GameManager gameManager;

    public int lastInputId = 0;

    //game data
    public Actor Actor
    {
        get
        {
            if (!gameManager.Actors.ContainsKey(actorId))
                return null;
            return gameManager.Actors[actorId];
        }
        set
        {
            actorId = value.Id;
        }
    }

    private int actorId = -1;

    public Client(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }
    public void Serialize(uLink.BitStream stream)
    {
        stream.Write(NetworkPlayer);
        stream.Write(actorId);
        stream.Write(lastInputId);
    }

    public void Deserialize(uLink.BitStream stream)
    {
        //NetworkPlayer = stream.Read<uLink.NetworkPlayer>();
        actorId = stream.Read<int>();
        lastInputId = stream.Read<int>();
    }
}
