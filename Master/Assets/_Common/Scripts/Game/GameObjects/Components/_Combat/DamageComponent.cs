using UnityEngine;
using System.Collections;

public class DamageComponent : ActorComponent 
{
    void Awake()
    {
        Actor.MessageBusManager.AddEventHandler<GameEvents.TakeDamageEvent>(TakeDamageEventHandler);
    }

    void TakeDamageEventHandler(object sender, MessageBus<GameEvents.TakeDamageEvent> message)
    {
        Actor.Attributes[ActorAttributes.Health] -= message.data.Damage;
        if (Actor.Attributes[ActorAttributes.Health] <= 0)
        {
            Actor.Game.MessageBusManager.SendMessage(new GameEvents.DeathEvent(message.data.Attacker, message.data.Receiver));
        }
    }
}
