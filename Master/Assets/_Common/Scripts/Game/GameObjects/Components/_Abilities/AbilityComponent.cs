using UnityEngine;
using System.Collections.Generic;

public class AbilityComponent : ActorComponent
{
    private Dictionary<int, BaseAbility> abilities = new Dictionary<int, BaseAbility>();

    void Start()
    {
        AbilityData primareyAbility = new AbilityData();
        primareyAbility.id = 1;
        abilities.Add(1, new MeleeAbility(Actor, primareyAbility));
        Actor.MessageBusManager.AddEventHandler<GameEvents.ActivateAbilityEvent>(ActivateAbilityEventHandler);
        Actor.MessageBusManager.AddEventHandler<GameEvents.InterruptAbilityEvent>(InterruptAbilityEventHandler);
        Actor.MessageBusManager.AddEventHandler<InputEvents.Input>(InputHandler);
    }

    void Update()
    {
        foreach (BaseAbility ability in abilities.Values)
        {
            ability.Update();
        }
    }

    void OnDestroy()
    {
        Actor.MessageBusManager.RemoveEventHandler<GameEvents.ActivateAbilityEvent>(ActivateAbilityEventHandler);
        Actor.MessageBusManager.RemoveEventHandler<GameEvents.InterruptAbilityEvent>(InterruptAbilityEventHandler);
    }

    public void InputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        if (message.data.PrimaryFire)
        {
            AbilityData primareyAbility = new AbilityData();
            primareyAbility.id = 1;
            GameEvents.ActivateAbilityEvent ability = new GameEvents.ActivateAbilityEvent(Actor, primareyAbility);
            Actor.Game.MessageBusManager.SendMessage(ability);
        }
    }

    private void ActivateAbilityEventHandler(object sender, MessageBus<GameEvents.ActivateAbilityEvent> message)
    {
        if (abilities.ContainsKey(message.data.Ability.id))
        {
            abilities[message.data.Ability.id].Start();
        }
    }

    private void InterruptAbilityEventHandler(object sender, MessageBus<GameEvents.InterruptAbilityEvent> message)
    {
        if (abilities.ContainsKey(message.data.Ability.id))
        {
            abilities[message.data.Ability.id].Interrupt();
        }
    }
}
