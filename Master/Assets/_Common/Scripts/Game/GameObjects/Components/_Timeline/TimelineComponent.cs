using UnityEngine;
using System.Collections.Generic;

public class TimelineComponent : ActorComponent 
{
    private Dictionary<int, TimelineObject> activeTimelines = new Dictionary<int, TimelineObject>();

	void Start () 
    {
        Actor.MessageBusManager.AddEventHandler<GameEvents.PlayTimelineEvent>(PlayTimelineEventHandler);
        Actor.MessageBusManager.AddEventHandler<GameEvents.EndTimelineEvent>(EndTimelineEventHandler);
	}

	void Update () 
    {
        List<TimelineObject> activeTimelinesTemp = new List<TimelineObject>(activeTimelines.Values);
        foreach (TimelineObject timeline in activeTimelinesTemp)
        {
            timeline.Update(Time.deltaTime);
        }
	}

    void OnDestroy()
    {
        Actor.MessageBusManager.RemoveEventHandler<GameEvents.PlayTimelineEvent>(PlayTimelineEventHandler);
        Actor.MessageBusManager.RemoveEventHandler<GameEvents.EndTimelineEvent>(EndTimelineEventHandler);
    }

    private void PlayTimelineEventHandler(object sender, MessageBus<GameEvents.PlayTimelineEvent> message)
    {
        if (activeTimelines.ContainsKey(message.data.Timeline.id))
            return;

        Debug.Log("play timeline");
        Actor.Attributes[ActorAttributes.Timeline, message.data.Timeline.id] = message.data.StartTime;
        TimelineObject timelineObject = new TimelineObject(message.data.StartTime, Actor, message.data.Timeline);
        
        activeTimelines.Add(message.data.Timeline.id, timelineObject);
    }

    private void EndTimelineEventHandler(object sender, MessageBus<GameEvents.EndTimelineEvent> message)
    {
        Actor.Attributes.RemoveAttributeValue(ActorAttributes.Timeline, message.data.Timeline.id);
        activeTimelines.Remove(message.data.Timeline.id);
    }
}
