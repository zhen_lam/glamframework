using UnityEngine;
using System.Collections;

public class PlayerModel : ActorComponent 
{
    public GameObject VisualModel { get; private set; }

    private string currentModel = "";
    void Awake()
    {
        Actor.MessageBusManager.AddEventHandler<GameEvents.ModelSwap>(ModelSwapHandler);
        Actor.MessageBusManager.AddEventHandler<AnimationEvents.AnimationEvent>(StartAnimationEventHandler);
        Actor.MessageBusManager.AddEventHandler<AnimationEvents.EndAnimationEvent>(EndAnimationEventHandler);
        Actor.MessageBusManager.AddEventHandler<AnimationEvents.SetAnimationTimeEvent>(SetAnimationTimeEventHandler);

        VisualModel = GameObject.Instantiate(Resources.Load("Characters/Player")) as GameObject;
        VisualModel.transform.parent = transform;
        VisualModel.transform.localPosition = Vector3.zero;
        VisualModel.transform.localRotation = Quaternion.identity;
    }

    public void FixedUpdate()
    {
        string model = Actor.Attributes[ActorAttributes.Model];
        if (model != currentModel)
        {
            LoadModel(model);
        }
    }

    public void ModelSwapHandler(object sender, MessageBus<GameEvents.ModelSwap> message)
    {
        Actor.Attributes[ActorAttributes.Model] = message.data.Model;
    }

    public void LoadModel(string model)
    {
        currentModel = model;
        StartCoroutine(AssetBundleManager.DownloadAsset(model, (bundle) =>
            {
                if (VisualModel != null)
                {
                    GameObject.Destroy(VisualModel);
                }
                VisualModel = GameObject.Instantiate(bundle.mainAsset) as GameObject;
                VisualModel.transform.parent = transform;
                VisualModel.transform.localPosition = Vector3.zero;
                VisualModel.transform.localRotation = Quaternion.identity;
            }));
    }

    public void StartAnimationEventHandler(object sender, MessageBus<AnimationEvents.AnimationEvent> message)
    {
        if (animation == null)
            return;

        AnimationState state = VisualModel.animation[message.data.AnimationName];
        state.layer = message.data.Layer;
        state.speed = message.data.Speed;
        state.wrapMode = message.data.WrapMode;
        state.blendMode = message.data.BlendMode;

        VisualModel.animation.Blend(message.data.AnimationName, message.data.Weight, message.data.FadeInTime);
    }

    public void EndAnimationEventHandler(object sender, MessageBus<AnimationEvents.EndAnimationEvent> message)
    {
        if (animation == null)
            return;

        VisualModel.animation.Blend(message.data.AnimationName, 0, message.data.FadeOutTime);
    }

    public void SetAnimationTimeEventHandler(object sender, MessageBus<AnimationEvents.SetAnimationTimeEvent> message)
    {
        if (animation == null)
            return;

        VisualModel.animation[message.data.AnimationName].time = message.data.Time;
    }

    void OnDestroy()
    {
        Actor.MessageBusManager.RemoveEventHandler<GameEvents.ModelSwap>(ModelSwapHandler);
    }
}
