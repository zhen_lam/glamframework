using UnityEngine;
using System.Collections;
using System;

public class ColliderCallbackComponent : MonoBehaviour 
{
    public Action<Collision> OnCollisionEnterCallback;
    public Action<Collision> OnCollisionStayCallback;
    public Action<Collision> OnCollisionExitCallback;
    public Action<Collider> OnTriggerEnterCallback;
    public Action<Collider> OnTriggerStayCallback;
    public Action<Collider> OnTriggerExitCallback;

    void Awake()
    {
        OnCollisionEnterCallback += (c) => { };
        OnCollisionStayCallback += (c) => { };
        OnCollisionExitCallback += (c) => { };
        OnTriggerEnterCallback += (c) => { };
        OnTriggerStayCallback += (c) => { };
        OnTriggerExitCallback += (c) => { };
    }

    void OnCollisionEnter(Collision collision)
    {
        OnCollisionEnterCallback(collision);
    }

    void OnCollisionStay(Collision collision)
    {
        OnCollisionStayCallback(collision);
    }

    void OnCollisionExit(Collision collision)
    {
        OnCollisionExitCallback(collision);
    }

    void OnTriggerEnter(Collider collider)
    {
        OnTriggerEnter(collider);
    }

    void OnTriggerStay(Collider collider)
    {
        OnTriggerStayCallback(collider);
    }

    void OnTriggerExit(Collider collider)
    {
        OnTriggerExitCallback(collider);
    }
}
