using UnityEngine;
using System.Collections.Generic;
using System;

public enum CollisionMapEnum
{
    Head,
    Pelvis,
    Spine1,
    Spine2,
    Spine3,
    RShoulder,
    RArm,
    RHand,
    LShoulder,
    LArm,
    LHand,
    RLeg,
    RCalf,
    RFoot,
    LLeg,
    LCalf,
    LFoot   
}

[System.Serializable]
public class CollisionMap
{
    public CollisionMapEnum id;
    public Collider collider;

    public ColliderCallbackComponent callbacks;
}

public class CollisionComponent : ActorComponent
{
    public List<CollisionMap> hitBoxes = new List<CollisionMap>();
    public List<CollisionMap> attackBoxes = new List<CollisionMap>();

    void Start()
    {
        Actor.MessageBusManager.AddEventHandler<GameEvents.CollisionTriggerEvent>(CollisionTriggerEventHandler);

        foreach (CollisionMap map in hitBoxes)
        {
            map.callbacks = map.collider.gameObject.AddComponent<ColliderCallbackComponent>();
        }

        foreach (CollisionMap map in attackBoxes)
        {
            map.callbacks = map.collider.gameObject.AddComponent<ColliderCallbackComponent>();
        }
    }

    void CollisionTriggerEventHandler(object sender, MessageBus<GameEvents.CollisionTriggerEvent> message)
    {
        for (int i = 0; i < attackBoxes.Count; i++)
        {
            if (attackBoxes[i].id == message.data.ColliderType)
            {
                attackBoxes[i].collider.enabled = message.data.Enabled;
            }
        }
    }

    public void RegisterTriggerCallback(CollisionMapEnum type, Action<Collider> OnEnter, Action<Collider> OnStay = null, Action<Collider> OnExit = null)
    {
        for (int i = 0; i < attackBoxes.Count; i++)
        {
            if (attackBoxes[i].id == type)
            {
                attackBoxes[i].callbacks.OnTriggerEnterCallback += OnEnter;
                attackBoxes[i].callbacks.OnTriggerStayCallback += OnStay;
                attackBoxes[i].callbacks.OnTriggerExitCallback += OnExit;
            }
        }
    }

    public void UnregisterTriggerCallback(CollisionMapEnum type, Action<Collider> OnEnter, Action<Collider> OnStay = null, Action<Collider> OnExit = null)
    {
        for (int i = 0; i < attackBoxes.Count; i++)
        {
            if (attackBoxes[i].id == type)
            {
                attackBoxes[i].callbacks.OnTriggerEnterCallback -= OnEnter;
                attackBoxes[i].callbacks.OnTriggerStayCallback -= OnStay;
                attackBoxes[i].callbacks.OnTriggerExitCallback -= OnExit;
            }
        }
    }

    public void RegisterCollisionCallback(CollisionMapEnum type, Action<Collision> OnEnter, Action<Collision> OnStay = null, Action<Collision> OnExit = null)
    {
        for (int i = 0; i < attackBoxes.Count; i++)
        {
            if (attackBoxes[i].id == type)
            {
                attackBoxes[i].callbacks.OnCollisionEnterCallback += OnEnter;
                attackBoxes[i].callbacks.OnCollisionStayCallback += OnStay;
                attackBoxes[i].callbacks.OnCollisionExitCallback += OnExit;
            }
        }
    }

    public void UnregisterCollisionCallback(CollisionMapEnum type, Action<Collision> OnEnter, Action<Collision> OnStay = null, Action<Collision> OnExit = null)
    {
        for (int i = 0; i < attackBoxes.Count; i++)
        {
            if (attackBoxes[i].id == type)
            {
                attackBoxes[i].callbacks.OnCollisionEnterCallback -= OnEnter;
                attackBoxes[i].callbacks.OnCollisionStayCallback += OnStay;
                attackBoxes[i].callbacks.OnCollisionExitCallback += OnExit;
            }
        }
    }
}
