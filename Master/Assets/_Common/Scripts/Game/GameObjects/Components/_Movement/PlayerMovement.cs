using UnityEngine;
using System.Collections.Generic;
using Rapid.Tools;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : ActorComponent 
{
    public Vector3 moveDirection;
    private CharacterController controller;

    void Awake()
    {
        Actor.MessageBusManager.AddEventHandler<InputEvents.Input>(InputHandler);
        controller = GetComponent<CharacterController>();
        if (controller == null)
        {
            controller = gameObject.AddComponent<CharacterController>();
        }
    }

    public void FixedUpdate()
    {

    }

    public void InputHandler(object sender, MessageBus<InputEvents.Input> message)
    {
        transform.localEulerAngles = message.data.ViewAngles;
        float y = moveDirection.y;
        Actor.Attributes[ActorAttributes.YVelocity] = y;
        float hor = message.data.Horizontal;
        float vert = message.data.Vertical;
        moveDirection = (transform.forward * vert + transform.right * hor) * Actor.Attributes[ActorAttributes.Speed, 20];
        moveDirection.y = y;


        if (!controller.isGrounded)
        {
            moveDirection.y -= Actor.Attributes[ActorAttributes.Gravity, 10] * message.data.DeltaTime;
        }


        if (message.data.Jump && controller.isGrounded)
        {
            moveDirection.y = Actor.Attributes[ActorAttributes.JumpSpeed, 10];
        }

        controller.Move(moveDirection * message.data.DeltaTime);

    }

    void OnDestroy()
    {
        Actor.MessageBusManager.RemoveEventHandler<InputEvents.Input>(InputHandler);
    }
}
