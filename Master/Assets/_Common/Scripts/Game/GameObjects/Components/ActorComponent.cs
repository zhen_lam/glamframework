using UnityEngine;
using System.Collections;

public class ActorComponent : MonoBehaviour 
{
    private Actor actor;

    public Actor Actor
    {
        get
        {
            if (actor == null)
                actor = GetComponent<ActorReference>().Actor;
            return actor;
        }
    }
}
