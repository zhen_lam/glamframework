using UnityEngine;
using System.Collections;

public class Actor
{
    private static int actorCounter = 0;
    public Game Game { get; private set; }
    private GameObject gameObject;

    public int Id { get; private set; }
    public string ActorType { get; private set; }
    public AttributeMap Attributes { get; private set; }
    public MessageBusManager MessageBusManager { get; private set; }

    public Vector3 position;
    public Quaternion rotation;

    public GameObject GameObject 
    { 
        get
        {
            if (gameObject == null)
            {
                gameObject = new GameObject("Actor" + Id, typeof(ActorReference));
                gameObject.GetComponent<ActorReference>().Actor = this;
            }
            return gameObject;
        }
        set
        {
            if (gameObject != null)
            {
                Debug.Log("destroy");
                GameObject.Destroy(gameObject);
            }

            gameObject = value;
            if (gameObject.GetComponent<ActorReference>() == null)
            {
                gameObject.AddComponent<ActorReference>();
            }
            gameObject.GetComponent<ActorReference>().Actor = this;
        }
    }

    public Actor(Game game, int id, string actorType)
    {
        Game = game;
        Id = id;
        ActorType = actorType;
        Attributes = new AttributeMap();
        MessageBusManager = new MessageBusManager();
    }

    public Actor(Game game, string actorType)
    {
        Game = game;
        ActorType = actorType;
        Id = actorCounter;
        actorCounter++;
        Attributes = new AttributeMap();
        MessageBusManager = new MessageBusManager();
    }
}
