﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class SyncUserCmd
{
    private static int syncCounter = 0;

    public int SyncId { get; private set; }

    public float deltaTime;
    public float Horizontal;
    public float Vertical;
    public bool Jump;
    public bool PrimaryFire;
    public bool SecondaryFire;

    public SyncUserCmd()
    {
        syncCounter++;
        SyncId = syncCounter;
    }

    public SyncUserCmd(int id)
    {
        SyncId = id;
    }
}

