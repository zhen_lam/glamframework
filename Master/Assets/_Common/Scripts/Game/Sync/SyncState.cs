using UnityEngine;
using System.Collections.Generic;

public class SyncState : IULinkSerializeable
{
    public static int syncCounter = 0;

    public int SyncId { get; private set; }
    public Dictionary<uLink.NetworkPlayer, Client> clients = new Dictionary<uLink.NetworkPlayer, Client>();
    public Dictionary<int, Actor> actors = new Dictionary<int, Actor>();

    private GameManager gameManager;

    public SyncState(GameManager gameManager)
    {
        this.gameManager = gameManager;
        clients = new Dictionary<uLink.NetworkPlayer, Client>(gameManager.Clients);
        actors = new Dictionary<int, Actor>(gameManager.Actors);
    }

    public void Serialize(uLink.BitStream stream)
    {
        SyncId = syncCounter;
        stream.Write(SyncId);
        
        stream.Write(actors.Count);
        foreach (Actor actor in actors.Values)
        {
            stream.Write(actor.Id);
            stream.Write(actor.ActorType);
            stream.Write(actor.GameObject.transform.position);
            stream.Write(actor.GameObject.transform.rotation);
            actor.Attributes.Serialize(stream);
        }

        stream.Write(clients.Count);
        foreach(KeyValuePair<uLink.NetworkPlayer, Client> kvp in clients)
        {
            kvp.Value.Serialize(stream);
        }

        syncCounter++;
    }

    public void Deserialize(uLink.BitStream stream)
    {
        SyncId = stream.Read<int>();
        HashSet<int> listOfActorsToKeep = new HashSet<int>();

        int count = stream.Read<int>();
        for (int i = 0; i < count; i++)
        {
            int Id = stream.Read<int>();

            listOfActorsToKeep.Add(Id);
            string actorType = stream.ReadString();
            if (!actors.ContainsKey(Id))
            {
                Actor actor = ActorFactoryManager.CreateActor(gameManager.Game, Id, actorType);
                actors.Add(Id, actor);
            }
            else
            {
                if (actors[Id].ActorType != actorType)
                {
                    actors[Id] = ActorFactoryManager.CreateActor(gameManager.Game, Id, actorType);
                }
            }
            actors[Id].position = stream.ReadVector3();
            actors[Id].rotation = stream.ReadQuaternion();
            actors[Id].Attributes.Deserialize(stream);
        }

        //delete actors that did not get synced
        Dictionary<int, Actor> tempActors = new Dictionary<int,Actor>(actors);
        foreach (KeyValuePair<int, Actor> kvp in tempActors)
        {
            if (listOfActorsToKeep.Contains(kvp.Key))
                continue;

            GameObject.Destroy(kvp.Value.GameObject);
            actors.Remove(kvp.Key);
        }

        HashSet<uLink.NetworkPlayer> listOfClientsToKeep = new HashSet<uLink.NetworkPlayer>();
        count = stream.Read<int>();
        for (int i = 0; i < count; i++)
        {
            uLink.NetworkPlayer player = stream.Read<uLink.NetworkPlayer>();
            listOfClientsToKeep.Add(player);

            if (!clients.ContainsKey(player))
            {
                clients.Add(player, new Client(gameManager));
            }
            clients[player].Deserialize(stream);
        }

        //delete clients that did not get synced
        Dictionary<uLink.NetworkPlayer, Client> tempClients = new Dictionary<uLink.NetworkPlayer, Client>(clients);
        foreach (KeyValuePair<uLink.NetworkPlayer, Client> kvp in tempClients)
        {
            if (listOfClientsToKeep.Contains(kvp.Key))
                continue;

            clients.Remove(kvp.Key);
        }
        syncCounter = SyncId;
    }
}
