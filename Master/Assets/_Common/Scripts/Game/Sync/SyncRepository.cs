﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class SyncRepository : IULinkSerializeable
{
    public CircularBuffer<SyncState> SyncStates = new CircularBuffer<SyncState>(100);
    public CircularBuffer<InputEvents.Input> SyncUserCmd = new CircularBuffer<InputEvents.Input>(100);

    public int ack = 0;

    private GameManager gameManager;

    public SyncRepository(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    public void Serialize(uLink.BitStream stream)
    {
        SyncState state = SyncStates.ElementAt(0);
        state.Serialize(stream);
    }

    public void Deserialize(uLink.BitStream stream)
    {
        SyncState state = new SyncState(gameManager);
        state.Deserialize(stream);
        ack = state.SyncId;
    }
}

