﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public interface IULinkSerializeable
{
    void Serialize(uLink.BitStream stream);
    void Deserialize(uLink.BitStream stream);
}

