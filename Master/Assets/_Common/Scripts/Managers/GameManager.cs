using UnityEngine;
using System.Collections.Generic;

public class GameManager 
{
    public Game Game { get; private set; }
    public Dictionary<uLink.NetworkPlayer, Client> Clients = new Dictionary<uLink.NetworkPlayer, Client>();
    public Dictionary<int, Actor> Actors = new Dictionary<int, Actor>();

    public GameManager(Game game)
    {
        Game = game;
    }
}
