using UnityEngine;
using System.Collections;

public class TimelineClip
{
    protected Actor actor;
    public TimelineClipData Data { get; private set; }
    protected float elapsedTime = 0;
    protected AttributeMap attributes;

    private bool hasStarted = false;
    private bool hasEnded = false;

    public TimelineClip(Actor actor, TimelineClipData data)
    {
        this.actor = actor;
        this.Data = data;
        attributes = new AttributeMap();
        for (int i = 0; i < data.attributes.Count; i++)
        {
            attributes.AddAttributeFromAttributeData(TimelineAnimationAttributes.Attributes, data.attributes[i]);
        }
    }

    public virtual void Start()
    {
        elapsedTime = 0;
        hasEnded = false;
    }

    public void Tick(float dt)
    {
        if (!hasStarted)
        {
            Start();
        }

        Update(dt);
        elapsedTime += dt;
        if (elapsedTime >= Data.length && !hasEnded)
        {
            hasEnded = true;
            End();
        }
    }

    public virtual void Update(float dt)
    {
    }

    public virtual void End()
    {
    }

    public virtual void SnapTo(float time)
    {
        elapsedTime = Mathf.Clamp(time, 0, Data.length);
    }
}
