using UnityEngine;
using System.Collections;

public class CollisionTimelineclip : TimelineClip
{
    public class CollisionTimelineclipFactory : ITimelineClipFactory
    {
        public int Type
        {
            get
            {
                return 1;
            }
        }

        public TimelineClip Create(Actor actor, TimelineClipData data)
        {
            return new CollisionTimelineclip(actor, data);
        }
    }

    public CollisionTimelineclip(Actor actor, TimelineClipData data)
        : base(actor, data)
    {

    }

    public override void Start()
    {
        actor.MessageBusManager.SendMessage(new GameEvents.CollisionTriggerEvent(actor, (CollisionMapEnum)attributes[TimelineCollisionAttributes.CollisionType], true));
        base.Start();
    }

    public override void Update(float dt)
    {
        base.Update(dt);
    }

    public override void End()
    {
        actor.MessageBusManager.SendMessage(new GameEvents.CollisionTriggerEvent(actor, (CollisionMapEnum)attributes[TimelineCollisionAttributes.CollisionType], false));

        base.End();
    }

    public override void SnapTo(float time)
    {
        base.SnapTo(time);
        actor.MessageBusManager.SendMessage(new AnimationEvents.SetAnimationTimeEvent(
            attributes[TimelineAnimationAttributes.Name],
            elapsedTime));
    }
}
