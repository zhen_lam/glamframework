using UnityEngine;
using System.Collections;

public class AnimationTimelineClip : TimelineClip
{
    public class AnimationTimelineClipFactory : ITimelineClipFactory
    {
        public int Type
        {
            get
            {
                return 0;
            }
        }

        public TimelineClip Create(Actor actor, TimelineClipData data)
        {
            return new AnimationTimelineClip(actor, data);
        }
    }

    public AnimationTimelineClip(Actor actor, TimelineClipData data)
        : base(actor, data)
    {

    }

    public override void Start()
    {
        actor.MessageBusManager.SendMessage(new AnimationEvents.AnimationEvent(
            attributes[TimelineAnimationAttributes.Name],
            attributes[TimelineAnimationAttributes.Speed],
            attributes[TimelineAnimationAttributes.Layer],
            attributes[TimelineAnimationAttributes.Weight],
            (WrapMode)attributes[TimelineAnimationAttributes.Wrapmode],
            (AnimationBlendMode)attributes[TimelineAnimationAttributes.Blendmode],
            attributes[TimelineAnimationAttributes.FadeinTime]));
        base.Start();
    }

    public override void Update(float dt)
    {
        base.Update(dt);
    }

    public override void End()
    {
        actor.MessageBusManager.SendMessage(new AnimationEvents.EndAnimationEvent(
            attributes[TimelineAnimationAttributes.Name], 
            attributes[TimelineAnimationAttributes.FadeoutTime]));

        base.End();
    }

    public override void SnapTo(float time)
    {
        base.SnapTo(time);
        actor.MessageBusManager.SendMessage(new AnimationEvents.SetAnimationTimeEvent(
            attributes[TimelineAnimationAttributes.Name],
            elapsedTime));
    }
}
