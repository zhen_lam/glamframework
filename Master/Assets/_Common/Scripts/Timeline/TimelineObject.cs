using UnityEngine;
using System.Collections.Generic;


public interface ITimelineClipFactory
{
    int Type { get; }
    TimelineClip Create(Actor actor, TimelineClipData data);
}

public class TimelineClipFactoryManager
{
    private static Dictionary<int, ITimelineClipFactory> factories = new Dictionary<int, ITimelineClipFactory>();

    static TimelineClipFactoryManager()
    {
        //TODO: use reflection to find all clip factories
        RegisterFactory(new AnimationTimelineClip.AnimationTimelineClipFactory());
    }

    public static void RegisterFactory(ITimelineClipFactory factory)
    {
        factories.Add(factory.Type, factory);
    }

    public static TimelineClip Create(Actor actor, TimelineClipData data)
    {
        return factories[data.clipType].Create(actor, data);
    }
}

public class TimelineObject 
{
    public int ID { get; private set; }
    public float StartTime { get; private set; }
    public Actor Actor { get; private set; }

    private TimelineData data;
    private List<TimelineClip> clips;

    private float elapsedTime = 0;

    public TimelineObject(float startTime, Actor actor, TimelineData data)
    {
        ID = data.id;
        StartTime = startTime;
        this.Actor = actor;
        this.data = data;
        clips = new List<TimelineClip>();
        for (int i = 0; i < data.clips.Count; i++)
        {
            Debug.Log("clips : " + data.clips[i].clipType);
            clips.Add(TimelineClipFactoryManager.Create(Actor, data.clips[i]));
        }

    }

    public void Update(float dt)
    {
        for (int i = 0; i < clips.Count; i++)
        {
            if (elapsedTime >= clips[i].Data.startTime)
                clips[i].Tick(dt);
        }
        elapsedTime += dt;
        if (elapsedTime >= data.length)
        {
            End();
        }
    }

    public void SnapTo(float time)
    {
        elapsedTime = time;
        for (int i = 0; i < clips.Count; i++)
        {
            clips[i].SnapTo(time - clips[i].Data.startTime);
        }
    }

    void End()
    {
        Actor.Attributes.RemoveAttributeValue(ActorAttributes.Timeline, ID);
        Actor.MessageBusManager.SendMessage(new GameEvents.EndTimelineEvent(Actor, data));
    }
}
