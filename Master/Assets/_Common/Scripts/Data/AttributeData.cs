using System.Collections.Generic;

[System.Serializable]
public class AttributeData  
{
    public int attributeData = 0;
    public int value = 0;
    public float valueF = 0;
    public string valueS = "";
}
