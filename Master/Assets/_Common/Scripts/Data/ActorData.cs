using UnityEngine;
using System.Collections.Generic;

public class ActorData 
{
    public int id = -1;
    public string actorType = "";
    public List<AttributeData> attributes = new List<AttributeData>();
    public List<int> abilities = new List<int>();
}
