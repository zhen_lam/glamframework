using System.Collections.Generic;

public class TimelineData 
{
    public int id = -1;
    public float length = 0;
    public List<TimelineClipData> clips = new List<TimelineClipData>();
}
