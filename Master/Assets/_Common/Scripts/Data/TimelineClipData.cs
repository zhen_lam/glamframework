using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class TimelineClipData  
{
    public int clipType = 0;
    public float length = 0;
    public float startTime = 0;
    public List<AttributeData> attributes = new List<AttributeData>();
}
