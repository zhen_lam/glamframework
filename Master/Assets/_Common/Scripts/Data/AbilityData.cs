using UnityEngine;
using System.Collections.Generic;

public class AbilityData 
{
    public int id = -1;
    public List<AttributeData> attributes = new List<AttributeData>();
}
