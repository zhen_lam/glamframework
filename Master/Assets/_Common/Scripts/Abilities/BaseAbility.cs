using UnityEngine;
using System.Collections;

public abstract class BaseAbility 
{
    protected AttributeMap attributes;
    public Actor Actor { get; private set; }

    public BaseAbility(Actor actor)
    {
        Actor = actor;
        attributes = new AttributeMap();
    }

	public abstract void Start();
    public abstract void Update();
    public abstract void End();
    public abstract void Interrupt();

    public void PlayTimeline(Actor actor, TimelineData data)
    {
        actor.MessageBusManager.SendMessage<GameEvents.PlayTimelineEvent>(new GameEvents.PlayTimelineEvent(Time.time, actor, data));
    }
}
