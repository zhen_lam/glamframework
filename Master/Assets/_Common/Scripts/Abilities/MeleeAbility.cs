using UnityEngine;
using System.Collections;

public class MeleeAbility : BaseAbility 
{
    private CollisionComponent collisionComponent;

    public MeleeAbility(Actor actor, AbilityData data)
        : base(actor)
    {
        collisionComponent = Actor.GameObject.GetComponent<CollisionComponent>();
    }

    public override void Start()
    {
        TimelineData timeline = new TimelineData();
        timeline.length = 5;

        TimelineClipData clip = new TimelineClipData();
        clip.startTime = 0;
        clip.length = 5;
        clip.clipType = 0;

        AttributeData data = new AttributeData();
        data.attributeData = 0;
        data.valueS = "idle";

        clip.attributes.Add(data);
        clip.clipType = 0;

        timeline.clips.Add(clip);

        Actor.MessageBusManager.SendMessage(new GameEvents.PlayTimelineEvent(0, Actor, timeline));
        collisionComponent.RegisterTriggerCallback(CollisionMapEnum.RHand, OnCollisionHandEnter);
    }

    public override void Update()
    {
    }

    public override void End()
    {
        collisionComponent.UnregisterTriggerCallback(CollisionMapEnum.RHand, OnCollisionHandEnter);
    }

    public override void Interrupt()
    {
    }

    void OnCollisionHandEnter(Collider collider)
    {
        Debug.Log("CollisionHandEnter");
    }
}
