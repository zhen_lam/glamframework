﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class MessageBusChannel<T> : IMessageBusChannel where T:EventArgs
{
    public List<EventHandler<MessageBus<T>>> eventHandlers = new List<EventHandler<MessageBus<T>>>();
    public List<MessageBus<T>> queuedMessages = new List<MessageBus<T>>();

    public void ProcessMessages()
    {
        for (int i = 0; i < queuedMessages.Count; i++)
        {
            for (int j = 0; j < eventHandlers.Count; j++)
            {
                if(!queuedMessages[i].isConsumed)
                    eventHandlers[j](null, queuedMessages[i]);
            }
        }
        queuedMessages.Clear();
    }

    public void AddEventHandler<J>(EventHandler<MessageBus<J>> eventHandler) where J:EventArgs
    {
        eventHandlers.Add(eventHandler as EventHandler<MessageBus<T>>);   
    }

    public void RemoveEventHandler<J>(EventHandler<MessageBus<J>> eventHandler) where J : EventArgs
    {
        eventHandlers.Remove(eventHandler as EventHandler<MessageBus<T>>);
    }

    public void AddMessage<J>(MessageBus<J> message) where J : EventArgs
    {
        queuedMessages.Add(message as MessageBus<T>);
    }

    public void SendMessage<J>(MessageBus<J> message) where J : EventArgs
    {
        for (int j = 0; j < eventHandlers.Count; j++)
        {
            if (!message.isConsumed)
                eventHandlers[j](null, message as MessageBus<T>);
        }
    }
}

