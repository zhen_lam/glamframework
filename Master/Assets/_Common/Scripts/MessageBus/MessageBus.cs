﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class MessageBus<T> : System.EventArgs where T:EventArgs
{
    public T data;
    public bool isConsumed = false;

    public MessageBus(T data)
    {
        this.data = data;
    }
}

