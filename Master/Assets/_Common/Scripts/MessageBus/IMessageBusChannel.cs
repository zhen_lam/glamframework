﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public interface IMessageBusChannel
{
    void ProcessMessages();
    void AddEventHandler<J>(EventHandler<MessageBus<J>> eventHandler) where J:EventArgs;
    void AddMessage<J>(MessageBus<J> message) where J : EventArgs;
    void SendMessage<J>(MessageBus<J> message) where J : EventArgs;
    void RemoveEventHandler<J>(EventHandler<MessageBus<J>> eventHandler) where J:EventArgs;
}

